import ast
import collections
from geralt.errors import DomainError, PathError
from unittest.case import skip
from geralt.dds import DDS
from typing import Deque, Dict, Set, Tuple, List, Union
import unittest
import textwrap
import functools
from geralt.domain import Domain
from geralt import util
from py2cfg.builder import CFGBuilder, CFG, Block, Link
import py2cfg
import random



def source(src):
    src = textwrap.dedent(src)
    def wrapper(fn):
        @functools.wraps(fn)
        def inner(obj):
            obj.cfg = CFGBuilder().build_from_src("", src)
            return fn(obj)
        return inner
    return wrapper


class Test_Domain(unittest.TestCase):

    def test_eq(self):
        self.assertNotEqual(Domain(0, 0), Domain(1, 1))
        self.assertEqual(Domain(1, 1), Domain(1, 1))
        self.assertNotEqual(Domain(0, 1), Domain(0, 1))
        self.assertEqual(Domain(1, 1), 1)

    def test_intersects(self):
        inter_a, inter_b = Domain(-20, 0), Domain(0, 10)
        self.assertTrue(Domain.intersects(inter_a, inter_b))
        self.assertTrue(Domain.intersects(inter_b, inter_a))
        not_inter_a, not_inter_b = Domain(-20, -10), Domain(10, 20)
        self.assertFalse(Domain.intersects(not_inter_a, not_inter_b))
        self.assertFalse(Domain.intersects(not_inter_b, not_inter_a))

    def test_intersection(self):
        l, d = Domain.intersection(Domain(-20, 0), Domain(0, 10))
        self.assertTrue(l.bot == l.top == 0)
        self.assertEqual(l, d)
        
        l, d = Domain.intersection(Domain(-20, 10.0), Domain(0, 20))
        self.assertTrue(l.bot == 0); self.assertTrue(l.top == 10)
        self.assertNotEqual(l, d)
        
        with self.assertRaises(ValueError):
            Domain.intersection(Domain(-20, -10), Domain(0, 10))

    def test_contains(self):
        self.assertTrue(0 in Domain(-1, 1))
        self.assertFalse(0 in Domain(1, 1))
        self.assertTrue(0 in Domain(0, 0))
        self.assertTrue(0 in Domain())
    
    def test_rich_compare(self):
        # Equivalent to:
        # A = random.randint(5, 10)
        # B = random.randint(0, 4)
        # A > B == True
        self.assertGreater(Domain(5, 10), Domain(0, 4))
        self.assertGreater(Domain(5, 10), 0)

        self.assertGreaterEqual(Domain(5, 10), Domain(0, 5))
        self.assertGreaterEqual(Domain(5, 10), -1)

        self.assertLess(Domain(0, 4), Domain(5, 10))
        self.assertLess(Domain(0, 4), 10)

        self.assertLessEqual(Domain(0, 5), Domain(5, 10))
        self.assertLessEqual(Domain(0, 5), 5)
        
         
    def test_get_split(self):
        l, d = Domain(-20, 20), Domain(-40, 30)
        self.assertEqual(Domain.get_split(l, d, 0), 0)
        self.assertEqual(Domain.get_split(l, d, 4), -5)

    def test_reduce_domain(self):
        # No intersection
        l, d = Domain(10, 20), Domain(-10, 0) 
        for ld, dd in Domain.reduce_domain(l, ast.Gt(), d):
            for i in range(10):
                lv, dv = random.randint(*ld), random.randint(*dd)
                self.assertGreater(lv, dv)

        with self.assertRaises(DomainError):
            # swap lvalue domain and rvalue domain
            for ld, dd in Domain.reduce_domain(d, ast.Gt(), l):
                pass
        
        # Intersects
        l, d = Domain(-10, 5), Domain(-5, 10)
        for ld, dd in Domain.reduce_domain(l, ast.Gt(), d):
            n = random.randint(-10, 10)
            if n in ld:
                self.assertNotIn(n, dd)
            elif n in dd:
                self.assertNotIn(n, ld)

        # Intersects
        l, d = Domain(-10, 0), Domain(0, 10)
        for ld, dd in Domain.reduce_domain(l, ast.GtE(), d):
            self.assertIn(0, ld)
            self.assertIn(0, dd)
            
            n = random.randint(-10, -1)
            self.assertNotIn(n, dd)
            n = random.randint(1, 10)
            self.assertNotIn(n, ld)
        
        l, d = Domain(-10, 5), Domain(-5, 10)
        for ld, dd in Domain.reduce_domain(l, ast.Lt(), d):
            n = random.randint(-10, 10)
            if n in ld:
                self.assertNotIn(n, dd)
            elif n in dd:
                self.assertNotIn(n, ld)
            else:
                assert 0


    def test_0(self):
        # Source:
        #   Offut et al. The Dynamic Domain Reduction Procedure. p 8-10.
        def min(x, y, z):
            mid = z # Start node
            if y < z:
                if x < y:
                    mid = y
                elif x < z:
                    mid = x # Goal node
            else:
                if x > y:
                    mid = y
                elif x > z:
                    mid = x
            return mid
        
        domX, domY, domZ = Domain(-10, 10), Domain(-10, 10), Domain(-10, 10)
        # y < z
        gen1_2 = domY.reduce_domain(ast.Lt(), domZ)
        try:
            domY, domZ = next(gen1_2)
        except:
            self.assert_(0)
        self.assertLess(domY, domZ)
        
        # x >= z
        gen2_3 = domX.reduce_domain(ast.GtE(), domY)
        try:
            domX, domY = next(gen2_3)
        except:
            self.assert_(0)
        self.assertGreaterEqual(domX, domY)
        
        # x < z
        gen3_5 = domX.reduce_domain(ast.Lt(), domZ)
        try:
            domX, domZ = next(gen3_5)
        except:
            self.assert_(0)

        self.assertLess(domX, domZ)
        x = random.randint(*domX)
        y = random.randint(*domY)
        z = random.randint(*domZ)
        
        v = min(x, y, z)
        self.assertIn(v, domX)
    
    def test_1(self):
        # Source:
        #   Offut et al. The Dynamic Domain Reduction Procedure. p 14-15.
        def value(a, b, c):
            # Start node
            if a < b:
                c = 16
                if a < c:
                    return a + 30
                else:
                    return a # Goal node
            else:
                c = 30
                return c + b + a

        domA, domB, domC = Domain(0, 20), Domain(10, 40), Domain(0, 100)
        generator_stack = collections.deque()

        # a < b
        generator1_2 = domA.reduce_domain(ast.Lt(), domB)
        try:
            domA, domB = next(generator1_2)
        except:
            self.assert_(0)
        else:
            generator_stack.appendleft(generator1_2)
        
        # c = 16
        domC = Domain(16, 16) 

        # (a >= c ) == not (a < c) 
        generator4_5 = domA.reduce_domain(ast.GtE(), domC)
        try:
            domA, domC = next(generator4_5)
            # No value in domA > 16
        except:
            # Go to the previous node
            gen = generator_stack.popleft()
            try:
                # chose another split
                domA, domB = next(gen)
            except:
                self.assert_(0)
            else:
                generator_stack.appendleft(gen)
        
        generator4_5 = domA.reduce_domain(ast.GtE(), domC)
        try:
            domA, domC = next(generator4_5)
        except:
            gen = generator_stack.popleft()
            try:
                domA, domB = next(gen)
            except:
                self.assert_(0)
            else:
                generator_stack.appendleft(gen)
        generator4_5 = domA.reduce_domain(ast.GtE(), domC)
        try:
            domA, domC = next(generator4_5)
        except:
            gen = generator_stack.popleft()
        self.assertGreaterEqual(domA, domC)
        for i in range(100):
            try:
                a = random.randint(domA.bot, domA.top)
                b = random.randint(int(domB.bot), domB.top)
                c = random.randint(domC.bot, domC.top)
                v = value(a, b, c)
            except:
                print(domA, domB, domC)
                self.assert_(0)
            else:
                self.assertIn(v, domA)
    



class Test_Expr(unittest.TestCase):
    cfg: CFG

    @source("""
        x = 10
    """)
    def test_const(self):
        asn: ast.Assign = self.cfg.entryblock.statements[0]
        tmpdds = collections.defaultdict(Domain)
        tmpdds[asn.targets[0].id] = Domain.expr_domain(asn.value, tmpdds)
        self.assertIn(10, tmpdds['x'])
    
    @source("""
        10 + 10
        10 - 5
        -1 * +10
        -(-10)
        10 / 1
        1 / 10
    """)
    def test_simple_binop(self):
        tmpdds = collections.defaultdict(Domain)
        statements: List[ast.stmt] = self.cfg.entryblock.statements
        dom = Domain.expr_domain(statements[0], tmpdds)
        self.assertEqual(dom, Domain(20, 20))
        dom = Domain.expr_domain(statements[1], tmpdds)
        self.assertEqual(dom, Domain(5, 5))
        dom = Domain.expr_domain(statements[2], tmpdds)
        self.assertEqual(dom, Domain(-10, -10))
        dom = Domain.expr_domain(statements[3], tmpdds)
        self.assertEqual(dom, Domain(10, 10))
        dom = Domain.expr_domain(statements[4], tmpdds)
        self.assertEqual(dom, Domain(10, 10))
        dom = Domain.expr_domain(statements[5], tmpdds)
        self.assertEqual(dom, Domain(1/10, 1/10))

    @source("""
        A + B
        (A + B) * (C - D)

    """)
    def test_3a(self):
        # Source:
        #   Offut et al. The Dynamic Domain Reduction Procedure. p 18.
        tmpdds = collections.defaultdict(Domain, {
            "A": Domain(0, 20),
            "B": Domain(10, 50),
        })
        statements: List[ast.stmt] = self.cfg.entryblock.statements
        dom = Domain.expr_domain(statements[0], tmpdds)
        self.assertEqual(hash(dom), hash(Domain(10, 70)))
    
    @source("""
        (A + B) * (C - D)
    """)
    def test_3b(self):
        # Source:
        #   Offut et al. The Dynamic Domain Reduction Procedure. p 18-20.
        tmpdds = DDS({
            "A": Domain(0, 20),
            "B": Domain(-50, 50),
            "C": Domain(10, 50),
            "D": Domain(30, 100),
        })
        statements: List[ast.stmt] = self.cfg.entryblock.statements
        dom = tmpdds.populate(statements)[0]
       
        self.assertEqual(hash(dom), hash(Domain(-3500, 2500)))
        return statements, tmpdds
    @skip
    def test_update(self):
        statements, tmpdds = self.test_3b()

        # Example 5
        new_dom = Domain(-500, 500)
        new_dom.update_domain(statements[0], tmpdds)  
        domA, domB, domC, domD = \
            tmpdds["A"], tmpdds["B"], tmpdds["C"], tmpdds["D"]
        
        new_dom.update_domain(statements[0], tmpdds)
        
        domA.validate(int)
        domB.validate(int)
        domC.validate(int)
        domD.validate(int)
        
        self.assertEqual(domA.bot, -11)
        self.assertEqual(domA.top, 11)
        self.assertEqual(domB.bot, -11)
        self.assertEqual(domB.top, 11)
        self.assertEqual(domC.bot, 22)
        self.assertEqual(domC.top, 44)
        self.assertEqual(domD.bot, 22)
        self.assertEqual(domD.top, 44)

    def test_update_fn1(self):
        cfgptr: List[CFG] = [None]
        @py2cfg.request_cfg(cfgptr)
        def fn1(arg0, arg1):
            a = arg0 + 3
            b = arg1 + 4
            if a + b < 10:
                return a
            else:
                return b

        cfg = cfgptr[0]
        linkblock_list: Deque[Union[ast.stmt, Block]] = collections.deque()
        links = cfg.find_path(cfg.finalblocks[1])
        for lk in reversed(links):
            linkblock_list.appendleft(lk.target) 
            if lk.exitcase is not None:
                linkblock_list.appendleft(lk.exitcase)
            linkblock_list.appendleft(lk.source)
        
        tmpdds = DDS({
            "arg0": Domain(-10, 10),
            "arg1": Domain(-10, 10),
        })
        populated: Set[Block] = set()
        for blk in linkblock_list:
            if isinstance(blk, Block):
                if blk not in populated:
                    tmpdds.populate(blk.statements)
                    populated.add(blk)
            else:
                for lexpr, op, dexpr in util.constraint_iterator(blk):
                    ld, dd = tmpdds[lexpr], tmpdds[dexpr]
                    domain_iter = Domain.reduce_domain(ld, op, dd)
                    left, right = next(domain_iter)
            
                    try:
                        while not (left.update_domain(lexpr, tmpdds)
                            and right.update_domain(dexpr, tmpdds)
                        ):
                            left, right = next(domain_iter)
                    except :
                        # infeasible
                        assert 0

        domA, domArg0, domArg1 = tmpdds["a"], tmpdds["arg0"], tmpdds["arg1"]
        domA.validate(int), domArg0.validate(int), domArg1.validate(int)
        for i in range(100):
            arg0 = random.randint(*domArg0)
            arg1 = random.randint(*domArg1) 
            r = fn1(arg0, arg1)
            self.assertIn(r, domA)


class Test_DDR(unittest.TestCase):

    def test_value_ddr(self):
        cfgptr: List[CFG] = [None]
        @py2cfg.request_cfg(cfgptr) 
        def value(a, b, c):
            # Start node
            if a < b:
                c = 16
                if a < c:
                    return a + 30
                else:
                    return a # Goal node
            else:
                c = 30
                return c + b + a
    
        cfg = cfgptr[0]
        last_dds = {
            "a": Domain(0, 20),
            "b": Domain(10, 40),
            "c": Domain(0, 100),
        }
        links = cfg.find_path(cfg.finalblocks[1])         
        datastack = util.DataStack(links)
        dds = datastack.process(last_dds)

        for i in range(100):
            a = random.randint(*dds["a"])
            b = random.randint(*dds["b"])
            c = random.randint(*dds["c"])
            r = value(a, b, c)
            self.assertIn(r, dds["a"])

    def test_min_ddr(self):
        cfgptr: List[CFG] = [None]
        # Source:
        #   Offut et al. The Dynamic Domain Reduction Procedure. p 8-10.
        @py2cfg.request_cfg(cfgptr)
        def min(x, y, z):
            mid = z # Start node
            if y < z:
                if x < y:
                    mid = y
                elif x < z:
                    mid = x # Goal node
                    return mid
            else:
                if x > y:
                    mid = y
                elif x > z:
                    mid = x
            return mid
        
        cfg = cfgptr[0]
        dds = util.DataStack(cfg.find_path(cfg.finalblocks[0])).process({
            "x": Domain(-10, 10),
            "y": Domain(-10, 10),
            "z": Domain(-10, 10),
        })

        for _ in range(100):
            x = random.randint(*dds["x"])
            y = random.randint(*dds["y"])
            z = random.randint(*dds["z"])
            
            r = min(x, y, z)
            self.assertEqual(r, x)
            

    def test_trityp_ddr(self):
        cfgptr: List[CFG] = [None]
        
        @py2cfg.request_cfg(cfgptr)
        def trityp(i, j, k):
            # trityp = 1 if triangle is scalene
            # trityp = 2 if triangle is isoceles
            # trityp = 3 if triangle is equilateral
            # trityp = 4 if not a triangle
            if (i <= 0
                or j <= 0
                or k <= 0
            ):
                trityp = 4
                return trityp
            trityp = 0
            if i == j:
                trityp = trityp + 1
            if i == k:
                trityp = trityp + 2
            if j == k: 
                trityp = trityp + 3
            if trityp == 0:
                if (i + j <= k
                    or j + k <= i
                    or i + k <= j
                ):
                    trityp = 4
                else:
                    trityp = 1
            return trityp

        cfg = cfgptr[0]
        datastack = util.DataStack(cfg.find_path(cfg.finalblocks[1]))



    
if __name__ ==  "__main__":
    print("\u001b[2J") # clear term of watchdog process
    unittest.main()