# geralt
Randomized assignment/Test data generation framework for Python

### Randomized Assignment Usage
Given a `<solution>` python source and a directory `<pattern>` to match against, mutate the py source and place in the matched directory.
```bash
$ python3 geralt/randomizer.py <solution> -p <pattern>
```

### Supported Mutagens<sub><sub>Incomplete list</sub></sub>
1. Arithmetic Operator Replacement (AOR)
    ```python
    a + b   # ast.Add
    a - b   # ast.Sub
    a * b   # ast.Mult
    a / b   # ast.Div
    a ** b  # ast.Pow
    a // b  # ast.FloorDiv
    ```
2. Relational Operator Replacement (ROR)
    ```python
    a == b  # ast.Eq
    a != b  # ast.Neq
    a <= b  # ast. LtE
    a >= b  # ast.GtE
    a < b   # ast.Lt
    a > b   # ast.Gt
    ```
