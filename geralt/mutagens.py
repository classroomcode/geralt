import ast
import random

mutagens = {}

# -- Implement mutagens as mixin classes -- #

class MutagenBase(ast.NodeTransformer):
    
    def __init__(self, difficulty:int):
        self.difficulty = difficulty
    
    def __init_subclass__(cls, **kwargs):
        if "name" not in kwargs:
            raise NameError(f"Unnamed mutagen {cls.__name__}")
        mutagens[kwargs["name"]] = cls
        return cls


class AOR_Mutagen(MutagenBase, name="AOR"):
    """Arithmetic Operator Replacement"""

    def visit_BinOp(self, node: ast.BinOp):
        if random.randint(0, 100) >= self.difficulty:
            op = random.choice((
                ast.Add,
                ast.Mult,
                ast.Sub,
                ast.Div,
                ast.Pow,
                ast.FloorDiv,
                ast.Mod,
            ))()
        else:
            op = node.op
        return ast.BinOp(self.visit(node.left), op, self.visit(node.right))


class ROR_Mutagen(MutagenBase, name="ROR"):
    """Relational Operator Replacement"""
    ROR = [
        ast.Eq,
        ast.NotEq,
        ast.LtE,
        ast.Lt,
        ast.GtE,
        ast.Gt,
    ]

    def _visit_ROR(self, node):
        if random.randint(0, 100) >= self.difficulty:
            return random.choice(self.ROR)()
        return node

    def visit_Eq(self, node: ast.Eq):
        return self._visit_ROR(node)

    def visit_NotEq(self, node: ast.NotEq):
        return self._visit_ROR(node)

    def visit_Lt(self, node: ast.Lt):
        return self._visit_ROR(node)

    def visit_LtE(self, node: ast.LtE):
        return self._visit_ROR(node)

    def visit_Gt(self, node:ast.Gt):
        return self._visit_ROR(node)

    def visit_GtE(self, node: ast.GtE):
        return self._visit_ROR(node)


class LCR_Mutagen(MutagenBase, name="LOR"):
    """Logical Connector Replacement"""

    def visit_And(self, node: ast.And):
        if random.randint(0, 100) >= self.difficulty:
            return ast.Or()
        return node

    def visit_Or(self, node: ast.Or):
        if random.randint(0, 100) >= self.difficulty:
            return ast.And()
        return node


class CLR_Mutagen(MutagenBase, name="CLR"):
    """Control Loop Replacement"""

    def visit_Continue(self, node:ast.Continue):
        if random.randint(0, 100) >= self.difficulty:
            return ast.Break()
        return node
    
    def visit_Break(self, node:ast.Break):
        if random.randint(0, 100) >= self.difficulty:
            return ast.Continue()
        return node
    
    def visit_Not(self, node):
        return node

# TODO
class UOI_Mutagen(MutagenBase, name="UOI"):
    """Unary Operator Insertion"""


class ABS_Mutagen(MutagenBase, name="ABS"):
    """Absolute Value Insertion"""

class CVR_Mutagen(MutagenBase, name="CVR"):
    """Constant Value Replacement"""