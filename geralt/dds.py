import ast
from collections import UserDict
import collections
from typing import Callable, Dict, Iterator, List
from geralt.domain import Domain
import functools


class DDS(UserDict, ast.NodeVisitor):
    """
    Domain Data Store: maps ast nodes to domains
    """
    # Instead of using the node as key, use the interior node.
    getkey = {
        ast.Name: lambda obj: obj.id,
        ast.Expr: lambda obj: obj.value,
    }

    # Namespace function. Methods decorated with this function are
    # intentionally empty. Saves a lot of repeat code.
    def overload(func):
        @functools.wraps(func)
        def method(self, obj, *args):
            getkey = type(self).getkey
            fn = getattr(super(DDS, self), func.__name__)
            for typeobj, getter in zip(getkey.keys(), getkey.values()):
                if isinstance(obj, typeobj):
                    return fn(getter(obj), *args)
            return fn(obj, *args)
        return method
    
    @overload
    def __contains__(self, obj): ...
    
    @overload
    def __getitem__(self, obj): ...

    @overload
    def __setitem__(self, obj, value): ...
  
    def __init__(self, mapping=None, assignments=None, **kwargs):
        # Copy on cast
        if mapping is not None:
            kwargs.update(mapping)
            for k, dom in zip(kwargs.keys(), kwargs.values()):
                kwargs[k] = Domain(dom.bot, dom.top)
        super().__init__(kwargs)
        self.assignments = collections.defaultdict(list) if assignments is None else assignments 
    def populate(self, statements: List[ast.stmt]) -> List["Domain"]:
        
        doms = list(filter(
            lambda x: x != -1, 
            (self.visit(stmt) for stmt in statements),
        ))
        assert not any(x is None for x in doms)
        return doms
    
    def generic_visit(self, node: ast.stmt):
        super().generic_visit(node)
        return -1 # sentinel value

    def visit_Expr(self, node: ast.Expr) -> Domain:
        return self.visit(node.value)

    def visit_Constant(self, node: ast.Constant) -> Domain:
        self[node] = Domain(node.value, node.value)
        return self[node]

    def visit_Name(self, node: ast.Name) -> Domain:
        return self[node]

    def visit_BinOp(self, node: ast.BinOp) -> Domain:
        op = Domain.switch_op[type(node.op)]
        ld = self.visit(node.left)
        dd = self.visit(node.right)
        self[node] = op(ld, dd)
        return self[node]
        
    def visit_Assign(self, node: ast.Assign):
        value_dom = self.visit(node.value)
        for target in node.targets:
            if not isinstance(target, ast.Name):
                raise NotImplementedError
            self[target] = value_dom
            self.assignments[target.id].append(node.value)
        return value_dom

    def visit_AnnAssign(self, node: ast.AnnAssign):
        raise NotImplementedError

    def visit_AugAssign(self, node: ast.AugAssign):
        raise NotImplementedError