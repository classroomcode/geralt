from numbers import Number
from typing import DefaultDict, Dict, Iterator, Optional, Tuple, Union
from geralt.errors import DomainError
import math
import ast



class Domain:
    bot: Number = -math.inf
    top: Number = math.inf
    flipped: bool
    is_const: bool

    def __init__(self,
        bot: Optional[Number] = None,
        top:Optional[Number] = None,
    ) -> None:
        self._flipped = False
        if isinstance(bot, Number) and isinstance(top, Number) and top < bot:
            tmp = top
            bot = top
            top = tmp
            self._flipped = True
        self.top = self.top if top is None else top
        self.bot = self.bot if bot is None else bot

    @property
    def is_const(self) -> bool:
        return self.top == self.bot

    def validate(self, fn):
        self.top = fn(self.top)
        self.bot = fn(self.bot)
        if not all(isinstance(x, Number) for x in self):
            raise TypeError("Validator function returned non-numerical value")
    @property
    def flipped(self):
        return self._flipped
    
    search_points = [1/2, 1/4, 3/4, 1/8, 3/8]

    def get_split(self, 
        other:"Domain",
        idx: int,
    ) -> Number:
        """
        other: right expr's domain with bot and top values
        returns: a value that divides a domain of value into two subdomains
        
        Adapted from Figure 4 of:
            A.J. Offut, Z. Jin and J.Pan, The Dynamic Domain Reduction Procedure
            for Test Data Generation
        """
        # Chose exp such that 2^exp <= srch_idx <= 2^exp + 1
        # exp = math.log(srch_idx, 2)
        # srch_pt = (2**exp - (2 * (2**exp - 1) - 1)) / 2**exp
        assert idx < len(self.search_points)
        srch_pt = self.search_points[idx]

        # try to equally split the left and right expression's domains
        lb, lt, rb, rt = self.bot, self.top, other.bot, other.top
        if lb >= rb and lt <= rt:
            split = (lt - lb) * srch_pt + lb
        elif lb <= rb and lt >= rt:
            split = (rt - rb) * srch_pt + rb
        elif lb >= rb and lt >= rt:
            split = (rt - lb) * srch_pt + lb
        else:
            assert lb <= rb and lt <= rt
            split = (lt - rb) * srch_pt + rb
        assert split >= lb
        assert split >= rb
        assert split <= lt
        assert split <= rt
        return split

    def intersects(self, other: "Domain") -> bool:
        if (self.bot == other.bot
            or self.bot == other.top
            or self.top == other.bot
            or self.top == other.bot
        ):
            return True
        
        elif self.bot < other.top:
            return self.top >= other.bot
        elif self.top > other.bot:
            return self.top <= other.bot

    def intersection(self, other: "Domain") -> Tuple["Domain", "Domain"]:
        if not self.intersects(other):
            raise ValueError(f"{self} does not intersect {other}")

        bot, top = min((self.top, other.top)), max((self.bot, other.bot))
        if bot > top:
            tmp = bot # Can't swap in place since values can be float
            bot = top
            top = tmp
        return Domain(bot, top), Domain(bot, top)
    
    def reduce_domain(self,
        operator: Union[ast.Eq, ast.NotEq, ast.Gt, ast.GtE, ast.Lt, ast.LtE],
        other: "Domain",  
    ) -> Iterator[Tuple["Domain", "Domain"]]:
        """Yield the left/right domain that satisfies the predicate."""
        
        if isinstance(operator, ast.Eq):
            # If 'self' and 'other' don't intersect, this path is infeasible
            # and ValueError is raised. Otherwise, return the intersection
            # of 'self' and 'other'
            yield self.intersection(other)
        
        elif isinstance(operator, ast.NotEq):
            # Already satisfies constraint
            if not self.intersects(other):
                yield self, other
            else:
                for i in range(len(self.search_points)):
                    split = self.get_split(other, i)
                    l0, l1 = Domain(self.bot, split+1), Domain(split+1, self.top)
                    d0, d1 = Domain(other.bot, split), Domain(split, other.top)
                    yield l0, d0
                    yield l0, d1
                    yield l1, d0
                    yield l1, d1

        elif isinstance(operator, ast.Gt):
            if not self.intersects(other):
                if self > other:
                    yield self, other
                else:
                    raise DomainError(f"! {self} > {other}")
            else:
                for i in range(len(self.search_points)):
                    split = math.ceil(self.get_split(other, i))
                    l0, l1 = Domain(self.bot, split + 1), Domain(split + 1, self.top)
                    d0, d1 = Domain(other.bot, split), Domain(split, other.top)
                    if l0 > d0:
                        yield l0, d0
                    if l0 > d1:
                        yield l0, d1
                    if l1 > d0:
                        yield l1, d0
                    if l1 > d1:
                        yield l1, d1

        elif isinstance(operator, ast.GtE):
            if not self.intersects(other):
                if self.bot > other.top:
                    yield self, other
                else:
                    raise DomainError(f"! {self} >= {other}")
            else:
                for i in range(len(self.search_points)):
                    split = math.ceil(self.get_split(other, i))
                    l0, l1 = Domain(self.bot, split), Domain(split, self.top)
                    d0, d1 = Domain(other.bot, split), Domain(split, other.top)
                    if l0 >= d0:
                        yield l0, d0
                    if l0 >= d1:
                        yield l0, d1
                    if l1 >= d0:
                        yield l1, d0
                    if l1 >= d1:
                        yield l1, d0

        elif isinstance(operator, ast.Lt):
            if not self.intersects(other):
                if self < other:
                    for _ in range(len(self.search_points)):
                        yield self, other
                else:
                    raise DomainError(f"! {self} < {other}")
            else:
                for i in range(len(self.search_points)):
                    split = math.floor(self.get_split(other, i))
                    l0, l1 = Domain(self.bot, split-1), Domain(split-1, self.top)
                    d0, d1 = Domain(other.bot, split), Domain(split, other.top)
                    if l0 < d0:
                        yield l0, d0
                    elif l0 < d1:
                        yield l0, d1
                    elif l1 < d0:
                        yield l1, d0
                    elif l1 < d1:
                        yield l1, d0

        elif isinstance(operator, ast.LtE):
            if not self.intersects(other):    
                if self < other:
                    yield self, other
                else:
                    raise DomainError(f"! {self} <= {other}")
            else:
                for i in range(len(self.search_points)):
                    split = math.floor(self.get_split(other, i))
                    l0, l1 = Domain(self.bot, split), Domain(split, self.top)
                    d0, d1 = Domain(other.bot, split), Domain(split, other.bot)
                    if l0 <= d0:
                        yield l0, d0
                    if l0 <= d1:
                        yield l0, d1
                    if l1 <= d0:
                        yield l1, d0
                    if l1 <= d1:
                        yield l1, d1


    def __hash__(self):
        return hash((self.bot, self.top))

    # -- Relational ops -- #
    # For domains A, B:
    #   A == B if any random number in A is equal to any random number in B
    #   A > B if any random number in A is > than any random number in B

    def __eq__(self, other: "Domain"):
        if isinstance(other, Number):
            return other == self.bot == self.top
        return self.bot == other.bot == self.top == other.top
    
    def __lt__(self, other: "Domain"):
        if isinstance(other, Domain):
            if self.intersects(other):
                return False
            return self.top < other.bot
        elif isinstance(other, Number):
            return self.top <= other
        return NotImplemented

    def __le__(self, other: "Domain"):
        if isinstance(other, Domain):
            return self.top <= other.bot and self.top == other.bot
        elif isinstance(other, Number):
            return self.top <= other
        return NotImplemented
    
    def __gt__(self, other: "Domain"):
        if isinstance(other, Domain):
            if self.intersects(other):
                return False
            return self.bot > other.top
        elif isinstance(other, Number):
            return self.bot > other
        return NotImplemented
        
    def __ge__(self, other: "Domain"):
        if isinstance(other, Domain):
            return self.bot >= other.top and self.bot == other.top 
        elif isinstance(other, Number):
            return self.bot >= other
        return NotImplemented
    
    def __contains__(self, item: Union[Number, "Domain"]):
        if isinstance(item, Domain):
            return item.bot >= self.bot and item.top <= self.top
        elif isinstance(item, Number):
            return self.bot <= item <= self.top
        else:
            return NotImplemented
        
    def __repr__(self):
        return f"Domain(bot={self.bot}, top={self.top})"
    
    def __index__(self):
        return math.floor((self.bot + self.top) / 2)

    def __bool__(self):
        return bool(self.bot and self.top)
            
    def __iter__(self):
        # star expr
        yield self.bot
        yield self.top

    def __add__(self, other: "Domain"):
        return Domain(self.bot + other.bot, self.top + other.top)

    def __sub__(self, other: "Domain"):
        return Domain(self.bot - other.bot, self.top - other.top)
    
    def __mul__(self, other: "Domain"):
        top = max((
            self.bot * other.bot,
            self.bot * other.top,
            self.top * other.bot,
            self.top * other.top,
        ))
        bot = min((
            self.bot * other.bot,
            self.bot * other.top,
            self.top * other.bot,
            self.top * other.top,
        ))
        return Domain(bot, top)
    
    def __div__(self, other: "Domain"):
        # python2.x
        top, bot = other.top, other.bot
        if top == 0:
            top = -1
        if bot == 0:
            bot == 1
        return Domain(self.bot/top, self.top/bot)
    
    def __truediv__(self, other: "Domain"):
        # python3.x
        return self.__div__(other)
    
    def _not_implemented(op):
        # Not a method
        raise NotImplementedError(f"Operator '{op}' not implemented")

    switch_op = {
        ast.Add: lambda a, b: a + b,
        ast.Sub: lambda a, b: a - b,
        ast.Mult: lambda a, b: a * b,
        ast.Div: lambda a, b: a / b,
        
        ast.FloorDiv: lambda a, b: _not_implemented("//"),
        ast.Mod: lambda a, b: _not_implemented("%"),
        ast.Pow: lambda a, b: _not_implemented("**"),
        ast.LShift: lambda a, b: _not_implemented("<<"),
        ast.RShift: lambda a, b: _not_implemented(">>"),
        ast.BitOr: lambda a, b: _not_implemented("|"),
        ast.BitXor: lambda a, b: _not_implemented("^"),
        ast.BitAnd: lambda a, b: _not_implemented("&"),
        ast.MatMult: lambda a, b: _not_implemented("@"),
    }

    switch_unary = {
        ast.UAdd: lambda a: a,
        ast.USub: lambda a: a * Domain(-1, -1),
        ast.Invert: lambda a: _not_implemented("~"),
        ast.Not: lambda a: _not_implemented("not")
    }

    switch_bool = {
        ast.And: lambda a, b: a and b,
        ast.Or: lambda a, b: a or b,
    }

    def _validate(self):
        self._flipped = False
        if self.top < self.bot:
            self._flipped = True
            tmp = self.top
            self.top = self.bot
            self.bot = tmp
    
    # The _update_OPERATOR methods are just functions declared within the
    # class namespace. They're not to be used as methods, nor can they be
    # decorated with staticmethod/classmethod.
    def _update_add(l, d, n, *args, **kwargs):
        if d.is_const:
            l.bot = n.bot - d.bot
            l.top = n.top - d.top
            return l._validate()

        bot, top = n.bot / 2, n.top / 2
        l.bot = d.bot = bot
        l.top = d.top = top
        l._validate()
        d._validate()

    def _update_sub(l, d, n, *args, i=0, **kwargs):
        # i = (..., -3, -2, -1, 0, 1, 2, 3, ...)
        l.bot = n.top + i*(n.top - n.bot) / 2
        l.top = (n.top - n.bot) / 2 + n.top + i*(n.top - n.bot) / 2
        d.bot = (n.top - n.bot) / 2 + i*(n.top - n.bot) / 2
        d.top = n.top - n.bot + i*(n.top - n.bot) / 2
        l._validate()
        d._validate()
    
    def _update_mult(l, d, n, expr, tmpdds, *args, **kwargs):
        if n.top >= 0 and n.bot >= 0:
            if tmpdds[expr].flipped:
                l.top = d.top = -math.sqrt(n.bot)
                l.bot = d.bot = -math.sqrt(n.top)
            else:
                l.top = d.top = math.sqrt(n.top)
                l.bot = d.bot = math.sqrt(n.bot)
        elif n.top < 0 and n.bot < 0:
            l.top = abs(n.top)
            l.bot = 1
            d.top = -1
            d.bot = n.bot / abs(n.top)
        elif n.bot < 0 and n.top >= 0:
            l.bot = -math.sqrt(abs(n.bot))
            l.top = min((
                math.floor(math.sqrt(abs(n.bot))),
                math.floor(n.top/math.sqrt(abs(n.bot)))
            ))
            d.bot = -min((
                math.floor(math.sqrt(abs(n.bot))),
                math.floor(n.top/math.sqrt(abs(n.bot)))
            ))
            d.top = math.sqrt(abs(n.bot))
        else:
            assert 0 and "Not reachable"
        
        l._validate()
        d._validate()
    
    def _update_div(l, d, n, *args, i=0, **kwargs):
        # i = (..., -3, -2, -1, 0, 1, 2, 3, ...)
        l.bot = (n.top+1) / (n.bot+1) * n.bot ** (i + 1) * n.top ** i
        l.top = n.bot**i * n.top**(i+1)
        d.bot = n.bot**i * n.top**i
        d.top = (n.top+1) / (n.bot+1) * n.bot**i * n.top**(i+1)
        l._validate()
        d._validate()

    switch_update_binop = {
        ast.Add: _update_add,
        ast.Sub: _update_sub,
        ast.Mult: _update_mult,
        ast.Div: _update_div,
    }

    
    @staticmethod
    def expr_domain(
            expr: ast.stmt,
            tmpdds: DefaultDict[Union[str, ast.stmt], "Domain"]
        ) -> "Domain":
        """
        Computes the domain of a given expression

        source:
            Offut et al. The Dynamic Domain Reduction Procedure for Test Data
            Generation. Figure 8
        """
        if isinstance(expr, ast.Assign):
            raise Exception("Assignment not allowed")
        elif isinstance(expr, ast.AnnAssign):
            raise Exception("Assignment not allowed")
        elif isinstance(expr, ast.AugAssign):
            raise Exception("Assignment not allowed")

        elif isinstance(expr, ast.Constant):
            return Domain(expr.value, expr.value)

        elif isinstance(expr, ast.Name):
            return tmpdds[expr.id]
        
        elif isinstance(expr, ast.Expr):
            return Domain.expr_domain(expr.value, tmpdds)
        
        elif isinstance(expr, ast.BinOp):
            op = Domain.switch_op[type(expr.op)]
            l = Domain.expr_domain(expr.left, tmpdds)
            d = Domain.expr_domain(expr.right, tmpdds)
            return op(l,d)

        elif isinstance(expr, ast.UnaryOp):
            op = Domain.switch_unary[type(expr.op)]
            dom = op(Domain.expr_domain(expr.operand, tmpdds))
            return dom
        else:
            raise NotImplementedError(f"Cannot handle {expr}")
    
    @staticmethod
    def constraint_domain(
        stmt: ast.stmt,
        tmpdds: dict
    ) -> Iterator[Tuple["Domain", "Domain"]]:
        if isinstance(stmt, ast.Compare):
            ld = Domain.expr_domain(stmt.left, tmpdds)
            for op, right in zip(stmt.ops, stmt.comparators):
                yield ld, Domain.expr_domain(right, tmpdds)

    def update_domain(
            self,
            expr,
            tmpdds:DefaultDict[Union[str, ast.stmt], "Domain"]
        ) -> bool:
        """
        Propagate changes to an expression's domain back down the
        expression tree

        Adapted from:
            Offut et al. The Dynamic Domain Reduction Procedure for Test Data
            Generation. Figure 9
        """
        if isinstance(expr, ast.Constant):
            return True
        
        if expr not in tmpdds:
            return False
  
        assert expr in tmpdds
        assert isinstance(tmpdds[expr], Domain)
        if isinstance(expr, ast.Expr):
            return self.update_domain(expr.value, tmpdds)
        
        if isinstance(expr, ast.BinOp):
            fn = self.switch_update_binop[type(expr.op)]
            lexpr, dexpr = expr.left, expr.right
            ldom, ddom = tmpdds[lexpr], tmpdds[dexpr]
            fn(ldom, ddom, self, expr, tmpdds)
            tmpdds[expr] = self
            return (ldom.update_domain(lexpr, tmpdds)
                and ddom.update_domain(dexpr, tmpdds))
    
        elif isinstance(expr, ast.Name):
            tmpdds[expr.id] = self
            for assignment in reversed(tmpdds.assignments[expr.id]):
                self.update_domain(assignment, tmpdds)
            return True
