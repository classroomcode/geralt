import astor
import argparse
import os
from pathlib import Path
from geralt.mutagens import mutagens

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    parser.add_argument(
        "-o",
        "--output",
        help="Output directory",
        default=".",
    )
    parser.add_argument("-d", "--difficulty",
        help="Probablility of mutation",
        type=int,
        default=50,
    )
    # The --pattern and [--output, --mutagens] options should be considered
    # mutually exclusive...
    parser.add_argument("-p", "--pattern",
        help="Place one mutant within each directory that matches",
        default=""
    )
    # TODO
    parser.add_argument("-t", "--transition",
        help="A csv file containing a state transition matrix",
        default=None,
    )
    parser.add_argument("--mutagens",
        help="Apply these mutagens",
        choices=[k for k in mutagens],
        nargs="*",
    )
    parser.add_argument("--mutants",
        help="Number of mutants to generate",
        default=1,
        type=int,
    )
    args = parser.parse_args()
    if not args.mutagens:
        bases = [v for v in mutagens.values()]
    else:
        bases = args.mutagens
    
    class Mutator(*bases, name="_"):
        def __init__(self, *args, **kwargs):
            for base in bases:
                base.__init__(self, *args, **kwargs)

    tree = astor.parse_file(args.file)
    canon = astor.to_source(tree)
    canon_file = os.path.join(os.path.dirname(args.file),
            os.path.splitext(os.path.basename(args.file))[0] + "_REF.py")
    with open(canon_file, "w") as fp:
        fp.write(canon)

    if args.pattern:
        for folder in Path(".").glob(args.pattern):
            canon_tree = astor.parse_file(canon_file)
            if os.path.isdir(folder):
                with open(
                    os.path.join(folder, os.path.basename(canon_file)), 
                    "w"
                ) as fp:
                    fp.write(astor.to_source(canon_tree))
    else:
        for i in range(args.mutants):
            canon_tree = astor.parse_file(canon_file)
            Mutator(args.difficulty).visit(canon_tree)
            with open(os.path.join(
                args.output,
                os.path.splitext(os.path.basename(args.file))[0] + f"_{i}.py"
            ), "w") as fp:
                fp.write(astor.to_source(canon_tree))
        

if __name__ == "__main__":
    main()