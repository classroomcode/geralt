import ast
import collections
from geralt.errors import DomainError, PathError
from typing import Iterator, List, Tuple, Optional, Dict
from collections import namedtuple
from geralt.dds import DDS
from geralt.domain import Domain
from py2cfg.model import Link

Constraint = namedtuple("Constraint", ["lexpr", "rel", "dexpr"])
Descriptor = namedtuple("Descriptor", ["type", "data"])
def constraint_iterator(stmt) -> Iterator[Constraint]:
    if isinstance(stmt, ast.Compare):
        lexpr = stmt.left
        for op, dexpr in zip(stmt.ops, stmt.comparators):
            yield Constraint(lexpr, op, dexpr)
    else: 
        raise NotImplementedError

control_flow_statements = {
    ast.For,
    ast.AsyncFor,
    ast.While,
    ast.Return,
    ast.Yield,
    ast.Try,
    ast.If,
    ast.Raise,
    ast.Await,
    ast.YieldFrom,
    ast.Call,
}

class DataStack:
    Item = collections.namedtuple("Item", [
        "statements",   # Stores statements that modify named variables
        "dds",          # Stores domains of named variables and expressions
        "predicate",    # Predicate needed to fulfill next constraint
        "reducer",      # Domain reducer
    ])

    def __init__(self, links: List[Link]) -> None:
        self.processed = collections.deque()
        def _path_gen():
            for link in links:
                if (data := list(filter(
                        lambda item: type(item) not in control_flow_statements,
                        link.source.statements,
                ))):
                    yield Descriptor("block", data)
                if link.exitcase is not None:
                    yield Descriptor(
                        "predicate",
                        list(constraint_iterator(link.exitcase),
                    ))
        
        def _item_gen():
            statements = []
            predicate = []
            for item in _path_gen():
                if item.type == "predicate":
                    # item.data should be a list of constraints in DNF
                    # which maps to a domain reducer in reducer list
                    predicate.extend(item.data)
                    yield predicate, statements
                    predicate, statements = [], []
                elif item.type == "block":
                    statements.extend(item.data)
                    
        self.unprocessed = [x for x in _item_gen()]
        self.iterctl = 0
        self.itermax = len(self.unprocessed)
    
    def __iter__(self):
        return self
    
    def __next__(self):
        if self.iterctl >= self.itermax:
            raise StopIteration
        
        predicate, statements = self.unprocessed[self.iterctl]
        def new_item(tmpdds):
            dds = DDS(tmpdds)
            dds.populate(statements)
            reducer = []
            for lexpr, op, dexpr in predicate:
                ldom, ddom = dds[lexpr], dds[dexpr]
                reducer.append(ldom.reduce_domain(op, ddom))
            return self.Item(statements, dds, predicate, reducer)
                
        return new_item    
    
    def _get(self) -> "Item":
        self.iterctl -= 1
        return self.processed.pop()

    def _put(self, item: "Item"):
        self.processed.append(item)
        self.iterctl += 1
    
    def process(self, dds):
        last_dds = dds
        for item_factory in self:
            init_item = item_factory(last_dds)
            
            def _process_item(item: self.Item = init_item): 
                nonlocal last_dds
                left: Domain
                right: Domain
                
                reducer = item.reducer[0]
                lexpr, _, dexpr = item.predicate[0]
                dds = item.dds        
                try:
                    left, right = next(reducer)
                    while not (
                        left.update_domain(lexpr, dds) and
                        right.update_domain(dexpr, dds)
                    ):
                        left, right = next(reducer)
                except DomainError:
                    _process_item(self._get())
                except StopIteration:
                    raise PathError("Infeasible path")
                else:
                    self._put(item)
                    last_dds = item.dds
            _process_item()
        return last_dds


    
    
    
