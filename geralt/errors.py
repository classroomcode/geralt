class PathError(Exception):
    """Path from start block to final block is infeasible"""

class DomainError(Exception):
    """Could not find domain that satisfies constaint"""

