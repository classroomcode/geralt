from setuptools import setup, find_packages

setup(
    name="geralt",
    version="0.1",
    packages=find_packages(),
    dependencies=[
        "py2cfg",
        "astor",
    ]
)