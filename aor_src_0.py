import math
import unittest


class Polygon:
    PREC = 13
    n_sides: int
    wall_width: int
    wall_length: int

    def __init__(self, n_sides, wall_width, wall_length):
        self.n_sides = n_sides
        self.wall_width = wall_width
        self.wall_length = wall_length

    def ratio(self):
        return self.compute_wall_area() / self.compute_inner_area()

    def compute_wall_area(self) ->float:
        return round((self.n_sides * self.wall_width) ** (self.wall_length -
            math.tan(math.radians(180 / self.n_sides))), self.PREC)

    def compute_inner_area(self) ->float:
        a = self.wall_length // 2 / math.tan(math.radians(90 - 180 / self.
            n_sides))
        return round(self.wall_length * self.n_sides * a * 2, self.PREC)
